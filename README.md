# CSCI 5410 Project

DalServerlessLMS lets authenticated users perform various operations like data processing, machine learning, sentiment analysis, chat online

-   _Date Created_: 07 Jul 2020
-   _Last Modification Date_: 06 Aug 2020

## Authors

-   [Avinash Gazula](av530575@dal.ca)
-   [Krupa Patel](Krupa.Patel@dal.ca)
-   [Mayank Bagla](Mak.B@dal.ca)
-   [Sreyas Naaraayanan Ramanathan](srey94@dal.ca)

## Getting Started

### Prerequisites

NodeJs must be installed on the system

### Installing

-   Clone the repository by using the provided GitLab link
-   Open the terminal and go to the directory where package.json is located
-   Install the node modules using the following command
    `npm install` or `npm i`
-   Start the react application by using the following command
    `npm start`

### Built With

-   [React](https://reactjs.org/) - The web framework used
-   [npm](https://www.npmjs.com/) - Dependency Management
-   [Bootstrap](https://getbootstrap.com/) - Used for styling

### Componenets Built

-   BotApp - Component for the Online Support Module
-   DataProcessing - Component for the Data Processing Module
-   Home Page - Home Page of the application
-   Login - Login Component
-   Register - Component for user registration
-   Online - Component for showing the users online
-   QnsAns - Component for 2FA
-   UploadFile - Component for Machine Learning module

## Deployment

App runs on localhost

### Github Link

https://github.com/avinashgazula/DALServerlessLMS-client

### Github Link for Serverless functions

https://github.com/avinashgazula/DALServerlessLMS-functions
