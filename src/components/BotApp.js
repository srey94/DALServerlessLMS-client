import LexChat from "react-lex";
import React, { Component } from "react";

class BotApp extends Component {
  render() {
    return (
      <LexChat
        botName="WebAppBot"
        IdentityPoolId="us-east-1:9159f85f-03f9-4131-aec0-1b7b8c9aeb7a"
        placeholder="Type Your Query Here"
        style={{ position: "absolute" }}
        backgroundColor="#FFFFFF"
        height="430px"
        headerText="Chatbot"
      />
    );
  }
}
export default BotApp;
