import React, { Component } from "react";
import "../components/User.css";
import { UploadFile } from "./UploadFile";
import BotApp from "./BotApp";
import NavBar from "./Navbar";

class Online extends Component {
  render() {
    return (
      <React.Fragment>
        <div>
          <NavBar />
        </div>
        <div>
          <div className="App-content">
            {
              <div
                style={{
                  fontSize: "20px",
                  paddingLeft: "125px",
                  paddingTop: "30px",
                  margin: "auto",
                  width: "50%",
                }}
              >
                Users page
              </div>
            }
            <UploadFile />
          </div>
          <BotApp></BotApp>
        </div>
      </React.Fragment>
    );
  }
}

export default Online;
