import { Button, Input } from "@material-ui/core";
import React, { useContext, useState } from "react";
import { GlobalContext } from "../context/GlobalState";
import BotApp from "./BotApp";

export const UploadFile = () => {
  const [images, setImages] = useState(null);

  const { cluster, addImage } = useContext(GlobalContext);

  const onSubmit = (event) => {
    event.preventDefault();
    let name = "";

    if (images) {
      var formatted_images = [];

      for (var i = 0; i < images.length; i++) {
        var formatted_image = {};
        formatted_image.file = images[i];
        formatted_image.filename = images[i].name;
        name = images[i].name;
        formatted_image.user = "user1";
        formatted_images.push(formatted_image);
      }

      console.log(formatted_images);
      // console.log(JSON.stringify(formatted_images[0]));

      const formData = new FormData();
      formData.append("count", images.length);
      for (var j = 0; j < images.length; j++) {
        formData.append(`file`, images[j]);
        formData.append(`filename`, images[j].name);
      }

      addImage(formData, name);
      event.target.value = null;
    }
  };

  return (
    <div>
      <div>
        <h3>Upload File</h3>
        <form onSubmit={onSubmit}>
          <div className="form-control">
            <label htmlFor="uploadedImage">File</label>
            <br />
            <Input
              id="uploadedImage"
              type="file"
              accept=".txt"
              onChange={(event) => {
                setImages(event.target.files);
              }}
            />
          </div>
          <br />
          <Button
            className="btn"
            variant="contained"
            color="primary"
            type="submit"
          >
            Upload
          </Button>
        </form>
        {cluster !== null ? (
          <p> file uploaded to cluster {cluster} </p>
        ) : (
          <p></p>
        )}
      </div>
      <BotApp></BotApp>
    </div>
  );
};
