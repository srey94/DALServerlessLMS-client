import React, { Component } from "react";
import NavBar from "./Navbar";
import BotApp from "./BotApp";

class Landing extends Component {
  render() {
    return (
      <div>
        <div>
          <NavBar />
        </div>

        <div>
          <h2>Welcome to Dalhousie University.</h2>
          <h4 style={{ marginBottom: "1rem" }}>
            This is our chatbot. You can input your queries.
          </h4>
        </div>
        <footer>
          <div>
            <BotApp />
          </div>
        </footer>
      </div>
    );
  }
}
export default Landing;
