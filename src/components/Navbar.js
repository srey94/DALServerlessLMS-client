import React, { Component } from "react";
import { Navbar, Nav } from "react-bootstrap";
import "./Navbar.css";
export default class NavBar extends Component {
  render() {
    return (
      <div class="navigation">
        <div class="topnav" id="myTopnav">
          <a href="/logout">Logout</a>
          <a href="/online">Online </a>
          <a href="/dataprocessing">Data Processing</a>
          <a href="/home">Home</a>
          <a href="/home">Dalhousie University LMS system</a>
        </div>
      </div>
    );
  }
}
