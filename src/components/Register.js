import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import axios from "axios";
import React, { Component } from "react";
import "../components/User.css";

class Register extends Component {
    state = {
        email: "",
        password: "",
        firstName: "",
        lastName: "",
        university: "",
        role: 1,
        sec_question: 1,
        answer: "",
        email_err: null,
        pass_err: null,
        fname_err: null,
        lname_err: null,
        university_err: null,
        answer_err: null,
        disable: true,
        signup_err: null,
    };

    register = () => {
        console.log(this.state.email);
        console.log(this.state.disable);

        let body = {
            email: this.state.email,
            password: this.state.password,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            university: this.state.university,
            sec_question: this.state.sec_question,
            role: this.state.role,
            answer: this.state.answer,
        };

        axios
            .post(
                `https://us-central1-severless-plumbing-user.cloudfunctions.net/user-regsitration`,
                body
            )
            .then((res) => {
                console.log(res);
                console.log(res.data);
                let resData = res.data;
                if (resData) {
                    this.props.history.push("/login");
                } else {
                    this.setState({
                        signup_err: true,
                    });
                }
            });
    };

    checkSubmit = () => {
        let valEmail = false;
        let passwordIsValid = false;
        let valFirstName = false;
        let vallastName = false;
        let valAnswer = false;

        //Email
        if (this.state.email === "") {
            this.setState({
                email_err: null,
            });
        } else {
            if (this.emailValidation(this.state.email)) {
                valEmail = true;
                this.setState({
                    email_err: null,
                });
            } else {
                this.setState({
                    email_err: "Enter valid email",
                });
            }
        }
        //Password
        if (this.state.password === "" || !this.state.password) {
            this.setState({
                passwordError: null,
            });
        } else {
            if (this.state.password.length >= 6) {
                passwordIsValid = true;
                this.setState({
                    pass_err: null,
                });
            } else {
                this.setState({
                    pass_err: "Password length should be at least 6 characters",
                });
            }
        }
        //FirstName

        if (this.state.firstName === "" || !this.state.firstName) {
            this.setState({
                firstNameError: null,
            });
        } else {
            if (this.state.firstName.length >= 1) {
                valFirstName = true;
                this.setState({
                    fname_err: null,
                });
            } else {
                this.setState({
                    fname_err: "Enter your first name",
                });
            }
        }
        //Second Name

        if (this.state.lastName === "" || !this.state.lastName) {
            this.setState({
                lname_err: null,
            });
        } else {
            if (this.state.lastName.length >= 1) {
                vallastName = true;
                this.setState({
                    lname_err: null,
                });
            } else {
                this.setState({
                    lname_err: "Enter your second name",
                });
            }
        }
        //Answers

        if (this.state.answer === "" || !this.state.answer) {
            this.setState({
                answer_err: null,
            });
        } else {
            if (this.state.answer.length >= 1) {
                valAnswer = true;
                this.setState({
                    answer_err: null,
                });
            } else {
                this.setState({
                    answer_err: "Enter your answer",
                });
            }
        }
        if (valEmail && passwordIsValid && valFirstName && vallastName) {
            this.setState({
                disable: false,
            });
        }
    };
    onValueChange = (e, label) => {
        const nextState = {};
        nextState[label] = e.target.value;
        this.setState(nextState);
    };
    emailValidation = (email) => {
        return new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(email);
    };

    render() {
        return (
            <React.Fragment>
                {/* <NavHeader /> */}
                <div className="App-content">
                    {
                        <div
                            style={{
                                fontSize: "20px",
                                paddingLeft: "125px",
                                paddingTop: "30px",
                                margin: "auto",
                                width: "50%",
                            }}
                        >
                            User Registration
            </div>
                    }

                    <div
                        style={{
                            paddingLeft: "125px",
                            paddingTop: "10px",
                            margin: "auto",
                            width: "50%",
                        }}
                    >
                        <div>
                            <div style={this.state.signup_err ? {} : { display: "none" }}>
                                <p>User Already exists</p>
                            </div>
                            <div className="space">
                                <TextField
                                    className="input-class"
                                    floatinglabeltext="Email"
                                    type="email"
                                    error={this.state.email_err !== null}
                                    helperText={this.state.email_err}
                                    onChange={(e) => this.onValueChange(e, "email")}
                                    id="standard-basic"
                                    required
                                    label="Email"
                                    variant="outlined"
                                    onBlur={this.checkSubmit}
                                />
                            </div>
                            <div className="space">
                                <TextField
                                    className="input-class"
                                    floatinglabeltext="Password"
                                    type="password"
                                    error={this.state.pass_err !== null}
                                    helperText={this.state.pass_err}
                                    onChange={(e) => this.onValueChange(e, "password")}
                                    id="standard-basic"
                                    required
                                    label="Password"
                                    variant="outlined"
                                    onBlur={this.checkSubmit}
                                />
                            </div>
                        </div>
                        <div className="space">
                            <TextField
                                className="input-class"
                                floatinglabeltext="First Name"
                                type="text"
                                error={this.state.fname_err !== null}
                                helperText={this.state.fname_err}
                                onChange={(e) => this.onValueChange(e, "firstName")}
                                id="standard-basic"
                                required
                                label="First Name"
                                variant="outlined"
                                onBlur={this.checkSubmit}
                            />
                        </div>

                        <div className="space">
                            <TextField
                                className="input-class"
                                floatinglabeltext="Last Name"
                                type="text"
                                error={this.state.lname_err !== null}
                                helperText={this.state.lname_err}
                                onChange={(e) => this.onValueChange(e, "lastName")}
                                id="standard-basic"
                                required
                                label="Last Name"
                                variant="outlined"
                                onBlur={this.checkSubmit}
                            />
                        </div>
                        <div className="space">
                            <TextField
                                className="input-class"
                                floatinglabeltext="University"
                                type="text"
                                error={this.state.university_err !== null}
                                helperText={this.state.university_err}
                                onChange={(e) => this.onValueChange(e, "university")}
                                id="standard-basic"
                                required
                                label="University"
                                variant="outlined"
                                onBlur={this.checkSubmit}
                            />
                        </div>
                        <div>
                            <FormControl>
                                <InputLabel id="demo-simple-select-label">Role</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.role}
                                    onChange={(e) => this.onValueChange(e, "role")}
                                >
                                    <MenuItem value={"Student"}>Student</MenuItem>
                                    <MenuItem value={"Instrcutor"}>Instrcutor</MenuItem>
                                </Select>
                            </FormControl>
                        </div>

                        <div>
                            <FormControl>
                                <InputLabel id="demo-simple-select-label">
                                    Security Question
                </InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    required
                                    label="Seucrity Question"
                                    value={this.state.sec_question}
                                    onChange={(e) => this.onValueChange(e, "sec_question")}
                                >
                                    <MenuItem value={"what is your pet's name?"}>
                                        what is your pet's name?
                  </MenuItem>
                                    <MenuItem value={"which city was your first school?"}>
                                        which city was your first school?
                  </MenuItem>
                                    <MenuItem value={">what was your favourite restraunt?"}>
                                        what was your favourite restraunt?{" "}
                                    </MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                        <div className="space">
                            <TextField
                                className="input-class"
                                floatinglabeltext="Answer"
                                type="text"
                                error={this.state.answer_err !== null}
                                helperText={this.state.answer_err}
                                onChange={(e) => this.onValueChange(e, "answer")}
                                id="standard-basic"
                                required
                                label="Answer"
                                variant="outlined"
                                onBlur={this.checkSubmit}
                            />
                        </div>
                        <div></div>
                        <div className="button-class">
                            <Button
                                disabled={this.state.disable}
                                onClick={this.register}
                                variant="contained"
                                color="primary"
                            >
                                Sign up
              </Button>
                        </div>

                        {
                            <div style={{ fontSize: "20px", paddingTop: "20px" }}>
                                <a href="/login">Already a Regsitered user? Login</a>
                            </div>
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
export default Register;
