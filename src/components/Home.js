import Button from "@material-ui/core/Button";
import React, { Component } from "react";
import "../components/User.css";

class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="App-content">
          {
            <div
              style={{
                fontSize: "20px",
                paddingLeft: "125px",
                paddingTop: "40px",
                margin: "auto",
                width: "50%",
              }}
            >
              <h1>Serverless LMS Application</h1>
            </div>
          }

          <div className="button-class">
            <Button
              onClick={() => this.props.history.push("/login")}
              variant="contained"
              color="secondary"
            >
              Login
            </Button>
          </div>
          <div className="button-class">
            <Button
              onClick={() => this.props.history.push("/register")}
              variant="contained"
              color="secondary"
            >
              Regsitration
            </Button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Home;
