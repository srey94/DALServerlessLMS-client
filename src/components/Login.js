import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import axios from "axios";
import React, { Component } from "react";

class Login extends Component {
  state = {
    email: "",
    password: "",
    email_err: null,
    pass_err: null,
    name_err: null,
    disable: true,
    signup_err: null,
  };
  checkSubmit = () => {
    let valEmail = false;
    let passwordIsValid = false;

    //Email
    if (this.state.email === "") {
      this.setState({
        email_err: null,
      });
    } else {
      if (this.emailValidation(this.state.email)) {
        valEmail = true;
        this.setState({
          email_err: null,
        });
      } else {
        this.setState({
          email_err: "Enter valid email",
        });
      }
    }
    //Password
    if (this.state.password === "" || !this.state.password) {
      this.setState({
        passwordError: null,
      });
    } else {
      if (this.state.password.length >= 6) {
        passwordIsValid = true;
        this.setState({
          pass_err: null,
        });
      } else {
        this.setState({
          pass_err: "Password length should be at least 6 characters",
        });
      }
    }

    if (valEmail && passwordIsValid) {
      if (this.state.name === "") {
        this.setState({
          name_err: "Please enter name",
        });
      } else if (valEmail && passwordIsValid) {
        this.setState({
          disable: false,
        });
      }
    }
  };

  onValueChange = (e, label) => {
    const nextState = {};
    nextState[label] = e.target.value;
    this.setState(nextState);
  };
  emailValidation = (email) => {
    return new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(email);
  };

  login = () => {
    console.log(this.state.email);
    // console.log(this.getSnapshotBeforeUpdate.password)
    let body = {
      email: this.state.email,
      password: this.state.password,
    };

    axios
      .post(
        `https://us-central1-severless-plumbing-user.cloudfunctions.net/user-login-level1`, body)
      .then((res) => {
        console.log(res);
        console.log(res.data);
        let resData = res.data;
        console.log("response data " + resData);
        if (resData.status === false) {
          console.log("fail " + resData.status);
          this.setState({
            signup_err: true,
          });
        } else {
          console.log("pass " + resData);
          console.log("question " + resData.question);

          localStorage.setItem("idToken", resData.idToken);
          localStorage.setItem("refreshToken", resData.refreshToken);
          localStorage.setItem("QuestionID", resData.QuestionID);
          localStorage.setItem("question", resData.question)
          localStorage.setItem("email", resData.email);
          this.props.history.push("/twofactor");
        }
      });
  };
  render() {
    return (
      <React.Fragment>
        <div className="App-content">
          {
            <div
              style={{
                fontSize: "20px",
                paddingLeft: "125px",
                paddingTop: "30px",
                margin: "auto",
                width: "50%",
              }}
            >
              Login Page
            </div>
          }

          <div
            style={{
              paddingLeft: "125px",
              paddingTop: "30px",
              margin: "auto",
              width: "50%",
              fontSize: "30px",
            }}
          >
            <div>
              <div style={this.state.signup_err ? {} : { display: "none" }}>
                <p>Inavalid user name or password</p>
              </div>
            </div>

            <div className="space">
              <TextField
                className="input-class"
                floatinglabeltext="Email"
                type="email"
                error={this.state.email_err !== null}
                helperText={this.state.email_err}
                onChange={(e) => this.onValueChange(e, "email")}
                id="standard-basic"
                required
                label="Email"
                variant="outlined"
                onBlur={this.checkSubmit}
              />
            </div>
            <div className="space">
              <TextField
                className="input-class"
                floatinglabeltext="Password"
                type="password"
                error={this.state.pass_err !== null}
                helperText={this.state.pass_err}
                onChange={(e) => this.onValueChange(e, "password")}
                id="standard-basic"
                required
                label="Password"
                variant="outlined"
                onBlur={this.checkSubmit}
              />
            </div>
            <div className="button-class">
              <Button
                disabled={this.state.disable}
                onClick={this.login}
                variant="contained"
                color="primary"
              >
                Login
              </Button>
            </div>

            {
              <div style={{ fontSize: "15px", paddingTop: "20px" }}>
                <a href="/register">New User? Register here</a>
              </div>
            }
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Login;
