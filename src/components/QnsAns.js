import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import axios from "axios";
import React, { Component } from "react";
import "../components/User.css";

class QnsAns extends Component {

  state = {
    question: '',
    user_answer: '',
    signup_err: null

  }

  componentDidMount() {
    this.setState({
      question: localStorage.getItem("question")
    })
  }
  onValueChange = (e, label) => {
    const nextState = {};
    nextState[label] = e.target.value;;
    this.setState(nextState);
  }

  twoFactor = () => {

    let body = {
      "email": localStorage.getItem("email"),
      "user_answer": this.state.user_answer
    }


    axios.post(`https://vpaxx9117a.execute-api.us-east-1.amazonaws.com/default/user-login-level2-lms`, body)
      .then(res => {
        console.log(res);
        console.log(res.data);
        console.log("question retrieved " + this.state.question)
        let resData = res.data;
        if (resData.status) {
          this.props.history.push("/online");
        } else {
          this.setState({
            signup_err: true
          })
        }
      })
  }



  render() {
    return (
      <React.Fragment>

        <div className="App-content" >
          <div style={this.state.questionLoad} >
            {<div style={{ fontSize: "20px", paddingLeft: "125px", paddingTop: "30px", paddingBottom: "30px", margin: "auto", width: "50%" }}>
              Security Question
          </div>}

            <div style={{ paddingLeft: "125px", paddingTop: "10px", margin: "auto", width: "50%", fontSize: "30px", }} >
              <div>
                <div style={this.state.signup_err ? {} : { display: 'none' }} >
                  <p>Incorrect answer</p>
                </div>
              </div>

            </div>


            <div className="space">

              <strong><p>Question: {this.state.question}</p></strong>
            </div>
            <div className="space">
              <TextField className="input-class"
                floatinglabeltext="Answer"
                type="text"
                onChange={e => this.onValueChange(e, 'user_answer')}
                id="standard-basic" required label="Answer"
                variant="outlined"
              />

            </div>
            <div className="button-class">
              < Button onClick={this.twoFactor} variant="contained" color="primary">
                Login
                    </Button>
            </div>


            {<div style={{ fontSize: "15px", paddingTop: "20px" }}>
              <a href="/register">New User? Register here</a>
            </div>}
          </div>
        </div>




      </React.Fragment >
    );
  }
}

export default QnsAns;
