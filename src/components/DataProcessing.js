import React from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Loader from "react-loader-spinner";
import NavBar from "./Navbar";
import BotApp from "./BotApp";

class Main extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      imageURL: "",
      isLoading: false,
      error: null,
      dis: true,
      imgState: true,
      loadState: false,
    };

    this.handleUploadImage = this.handleUploadImage.bind(this);
  }

  handleUploadImage(ev) {
    ev.preventDefault();

    // const data = new FormData();
    // data.append('file', this.uploadInput.files[0]);
    // // data.append('filename', this.fileName.value);
    // fetch('http://localhost:8000/upload', {
    //   method: 'POST',
    //   body: data,
    // }).then((response) => {
    //   response.json().then((body) => {
    //         // this.setState({ imageURL: `http://localhost:8000/simple.png` });
    //   });
    // });

    const data = new FormData();
    data.append("file", this.uploadInput.files[0]);

    axios
      .post(`https://data-processing-b7x6o6mozq-uc.a.run.app/upload`, data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res) => {
        toast.success("upload success");
        this.setState({ imageURL: res.data, dis: false });
      })
      .catch((err) => {
        toast.error("upload fail");
      });
  }

  imageChange = (ev) => {
    ev.preventDefault();
    const data = new FormData();
    data.append("file", this.uploadInput.files[0].name);
    this.setState({ isLoading: true, loadState: true });

    axios
      .get(`https://data-processing-b7x6o6mozq-uc.a.run.app/try`, {
        params: { filename: this.uploadInput.files[0].name },
      })
      .then((res) => {
        this.setState({
          imageURL: res.data,
          isLoading: false,
          imgState: false,
          loadState: false,
        });
      })
      .catch((error) => this.setState({ error, isLoading: false }));
  };

  render() {
    const { isLoading, error } = this.state;
    if (error) {
      return <p>{error.message}</p>;
    }
    // if (isLoading) {
    //   return <p>Loading ...</p>;
    // }
    return (
      <div>
        <div>
          <NavBar></NavBar>
        </div>
        <h2>Data Processing</h2>
        <form onSubmit={this.handleUploadImage}>
          <div>
            <input
              ref={(ref) => {
                this.uploadInput = ref;
              }}
              type="file"
            />
          </div>
          <div class="form-group">
            <ToastContainer />
          </div>
          {/* <div>
                <input ref={(ref) => { this.fileName = ref; }} type="text" placeholder="Enter the desired name of file" />
                </div> */}
          <br />
          <div>
            <button>Upload</button>
          </div>
        </form>
        <br />
        <form onSubmit={this.imageChange}>
          <button disabled={this.state.dis}>Get Word Cloud</button>
          <br />
          <div>
            <div
              style={{
                width: "100%",
                height: "100",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Loader
                visible={this.state.loadState}
                type="ThreeDots"
                color="#2BAD60"
                height="100"
                width="100"
              />
            </div>
            <img
              hidden={this.state.imgState}
              src={`data:image/jpeg;base64,${this.state.imageURL}`}
              alt="img"
            />
          </div>
        </form>
        <footer>
          <div>
            <BotApp />
          </div>
        </footer>
      </div>
    );
  }
}

export default Main;
