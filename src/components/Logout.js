import Button from "@material-ui/core/Button";
import React, { Component } from "react";
import axios from 'axios';

class Logout extends Component {


    state = {
        question: '',
        user_answer: '',
        signup_err: null

    }
    logout = () => {

        let body = {
            "email": localStorage.getItem("email")

        }


        axios.post(`https://yj368cuf9g.execute-api.us-east-1.amazonaws.com/default/user-logout-lms`, body)
            .then(res => {
                console.log(res);
                console.log(res.data);

                let resData = res.data;
                if (resData.status === true) {
                    localStorage.clear()
                    this.props.history.push("/");
                } else {
                    this.setState({
                        signup_err: true
                    })
                }
            })
    }


    render() {
        return (
            <React.Fragment>
                <div className="App-content" >
                    {/* {<div style={{ fontSize: "20px", paddingLeft: "125px", paddingTop: "40px", margin: "auto", width: "50%" }}>
                        Serveless LMS Application
          </div>} */}
                    <div>
                        <div style={this.state.signup_err ? {} : { display: "none" }}>
                            <p>Sorry Logout Failed</p>
                        </div>
                    </div>
                    <div className="button-class">

                        <Button onClick={this.logout} style={{ float: 'centre', marginTop: "20px"}} variant="contained" color="secondary" >
                            Logout
              </Button>
                    </div>

                    {
                        <div
                            style={{
                                fontSize: "20px",
                                paddingLeft: "125px",
                                paddingTop: "30px",
                                margin: "auto",
                                width: "50%",
                            }}
                        >
                            You are about to logout! Click the button to logout!!
            </div>
                    }

                </div>

            </React.Fragment >
        );
    }


}

export default Logout;