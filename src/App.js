import React from "react";

import "./App.css";

import Login from "./components/Login";
import Register from "./components/Register";
import QnsAns from "./components/QnsAns";
import Online from "./components/Online";
import Home from "./components/Home";
import DataProcessing from "./components/DataProcessing";
import Landing from "./components/Landing";
import Logout from "./components/Logout";

import { BrowserRouter as Router, Route } from "react-router-dom";
import { GlobalProvider } from "./context/GlobalState";

function App() {
  return (
    <GlobalProvider>
      <Router>
        <div className="App">
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/twofactor" component={QnsAns} />
          <Route exact path="/online" component={Online} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/" component={Home} />
          <Route exact path="/home" component={Landing} />
          <Route exact path="/dataprocessing" component={DataProcessing} />



          {/* <Route exact path="/home" component={Home} /> */}
        </div>
      </Router>
    </GlobalProvider>
  );
}

export default App;
