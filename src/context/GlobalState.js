import React, { createContext, useReducer } from "react";
import AppReducer from "./AppReducer";
import axios from "axios";

const initialState = {
  user: [],
  cluster: null,
  words: null,
  currentWord: null,
  error: null,
  success: null,
};

export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  async function registerUser(user) {
    // try {
    //     const res = await axios.post(
    //         "https://register-service-njeftlxetq-de.a.run.app/api/register",
    //         {
    //             name: user.name,
    //             email: user.email,
    //             password: user.password,
    //         }
    //     );
    //     dispatch({
    //         type: "ADD_USER",
    //         payload: res.data.success,
    //     });
    //     return res.data.success;
    // } catch (error) {
    //     console.log(error);
    //     dispatch({
    //         type: "ERROR_USER",
    //         payload: error,
    //     });
    // }
    return false;
  }

  async function verifyUser(user) {
    // try {
    //     const res = await axios.post(
    //         "https://login-service-njeftlxetq-de.a.run.app/api/login",
    //         {
    //             email: user.email,
    //             password: user.password,
    //         }
    //     );

    //     dispatch({
    //         type: "LOGIN_USER",
    //         payload: res.data.success,
    //     });
    //     return res.data.success;
    // } catch (error) {
    //     console.log(error);
    //     dispatch({
    //         type: "ERROR_USER",
    //         payload: error,
    //     });
    // }

    return false;
  }

  async function getActiveUsers() {
    // try {
    //     const res = await axios.get(
    //         "https://session-service-njeftlxetq-de.a.run.app/api/session/activeusers"
    //     );

    //     dispatch({
    //         type: "ACTIVE_USERS",
    //         payload: res.data.users,
    //     });
    //     return res.data.success;
    // } catch (error) {
    //     console.log(error);
    //     dispatch({
    //         type: "ERROR_USER",
    //         payload: error,
    //     });
    // }

    return false;
  }

  async function logoutUser(email) {
    // try {
    //     const res = await axios.post(
    //         "https://session-service-njeftlxetq-de.a.run.app/api/session/logout",
    //         { email }
    //     );

    //     dispatch({
    //         type: "LOGOUT_USER",
    //         payload: res.data.success,
    //     });
    //     return res.data.success;
    // } catch (error) {
    //     console.log(error);
    //     dispatch({
    //         type: "ERROR_USER",
    //         payload: error,
    //     });
    // }

    return false;
  }

  async function addImage(images, name) {
    try {
      const res = await axios.post(
        "https://cors-anywhere.herokuapp.com/https://us-east1-indigo-bedrock-284319.cloudfunctions.net/getFileClusters",
        images,
        { params: { message: name } }
      );
      console.log(res.data);
      dispatch({
        type: "ADD_IMAGE",
        cluster: res.data.cluster,
        words: res.data.wordClusters,
        currentWord: res.data.word,
      });
    } catch (error) {
      dispatch({
        type: "ERROR__IMAGE",
        payload: error,
      });
    }
  }

  return (
    <GlobalContext.Provider
      value={{
        success: state.success,
        user: state.user,
        error: state.error,
        cluster: state.cluster,
        words: state.words,
        currentWord: state.currentWord,
        addImage,
        registerUser,
        verifyUser,
        getActiveUsers,
        logoutUser,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
